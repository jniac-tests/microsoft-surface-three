import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import React, { useRef } from 'react'
import { useFrame, useLoader, useThree } from 'react-three-fiber'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'
import * as loaders from './loaders'

export const SkyBox = () => {
  const { scene, gl:renderer } = useThree()
  const url = 'data/startup_Version_01.hdr'
  loaders.loadCubeMap(url, renderer).then(texture => {
    scene.background = texture
  })
  loaders.loadCubeMap('data/quattro_canti_1k.hdr', renderer).then(texture => {
    scene.environment = texture
  })
  return null
}

const Model = () => {
  const url = 'data/SurfaceBook15_03.glb'
  const gltf = useLoader(GLTFLoader, url)
  gltf.scene.scale.setScalar(4)
  return <primitive object={gltf.scene} position={[0, 0, 0]} />
}
const Box = () => (
  <mesh>
    <boxGeometry/>
    <meshNormalMaterial/>
  </mesh>
)
const SafeModel = () => {
  return (
    <React.Suspense fallback={<Box/>}>
      <Model/>
    </React.Suspense>
  )
}
const Icotube = () => {
  const url = 'data/icotube.fbx'
  const fbx = useLoader(FBXLoader, url)
  fbx.traverse(obj => {
    if (obj instanceof THREE.Mesh) {
      obj.material = new THREE.MeshPhysicalMaterial({
        color: '#456',
        clearcoat: 1,
        clearcoatRoughness: .2,
        roughness: .4,
      })
    }
  })
  fbx.scale.setScalar(.05)
  return <primitive object={fbx} position={[0, 0, 0]} />
}

const sphere = new THREE.IcosahedronGeometry(.5, 12)
const Sphere = ({ 
  color = '#306', 
  scale = 1, 
  position, 
  delta = 0,
  ...props 
}) => {
  position = new THREE.Vector3(...position)
  const mesh = useRef()
  useFrame((state) => {
    let { x, y, z } = position
    y += Math.sin(delta * Math.PI + state.frames / 1000)
    mesh.current.position.set(x, y, z)
    mesh.current.scale.setScalar(scale)
    state.invalidate()
  })
  return (
    <mesh ref={mesh} {...props}>
      <bufferGeometry {...sphere}/>
      <meshPhysicalMaterial
        color={color}
        roughness={0.5}
        clearcoatRoughness={0}
        clearcoat={1}
      />
    </mesh>
  )
}

const Setup = () => {
  const { camera, gl } = useThree()
  camera.near = 0.01
  camera.position.set(-0.50, 0.66, 1.23)
  camera.lookAt(new THREE.Vector3())
  camera.updateProjectionMatrix()
  const controls = new OrbitControls(camera, gl.domElement)
  controls.target.set(-0.03, 0.23, -0.15)
  controls.update()
  Object.assign(window, { camera, controls })
  return null
}

export const Stage = () => {
  const showIcotube = false
  return (
    <React.Suspense fallback={null}>
      <Setup/>
      {/* <OrbitControls dampingFactor={1}/> */}
      {/* <ambientLight/> */}
      <SkyBox/>
      <Sphere position={[-1, 1, -1.4]} delta={0.0} scale={.75} color="#fc0"/>
      <Sphere position={[0, 1, -1.4]} delta={0.5}scale={.5} color="#f30"/>
      <Sphere position={[1, 1, -1.4]} delta={1.0}/>
      <SafeModel/>
      {showIcotube && (
        <React.Suspense fallback={null}>
          <Icotube/>
        </React.Suspense>
      )}
    </React.Suspense>
  )
}