import { DoubleMap, during, MultipleMap, tween } from './Animator'
import tap from 'tap'

const testDoubleMap = () => {
  tap.test('DoubleMap', t => {

    const dm = new DoubleMap<object, string, number>()
    const target1 = { name:'target1' }
    const target2 = { name:'target2' }
    
    t.test('set, get, getMap', t => {

      dm.set(target1, 'one', NaN)
      dm.set(target1, 'one', 1)
      dm.set(target1, 'two', 2)
      t.equal(dm.size, 2)
      t.equal(dm.get(target1, 'one'), 1)
      
      // test clear
      dm.clear()
      t.equal(dm.getMap(target1), undefined)
      t.equal(dm.size, 0)
      
      // test delete
      dm.set(target1, 'one', 1)
      dm.set(target1, 'two', 2)
      t.equal(dm.getMap(target1)?.size, 2)
      dm.delete(target1, 'one')
      dm.delete(target1, 'two')
      // no map should be associated with target1, since there are no remaining 
      // values (the previous map should be deleted with the removal of the last value)
      t.equal(dm.getMap(target1), undefined)
      t.equal(dm.size, 0)

      t.end()
    })
    
    t.test('deleteAll', t => {
      
      dm.set(target1, 'one', 1)
      dm.set(target1, 'two', 2)
      dm.set(target2, 'one', 1)
      dm.set(target2, 'two', 2)
      t.equal(dm.size, 4)
      t.equivalent([...dm.getMap(target1)!.keys()], ['one', 'two'])
      dm.deleteAll(target1)
      t.equal(dm.getMap(target1), undefined)
      t.equal(dm.size, 2)

      t.end()
    })
    
    t.end()
  })
}

const testMultipleMap = () => {
  tap.test('MultipleMap', t => {
    const mm = new MultipleMap<object,string>()
    const target1 = { name:'target1' }
    const target2 = { name:'target2' }

    mm.add(target1, 'one')
    mm.add(target1, 'two')
    mm.add(target2, '1')
    mm.add(target2, '2')
    
    t.equal(mm.size, 4)
    t.equivalent([...mm.get(target1)!], ['one', 'two'])

    mm.delete(target1, 'one')
    t.notEqual(mm.get(target1), undefined)
    mm.delete(target1, 'two')
    t.equal(mm.get(target1), undefined)
    t.equal(mm.size, 2)
    
    mm.deleteAll(target2)
    t.equal(mm.get(target2), undefined)
    t.equal(mm.size, 0)

    t.end()
  })
}

const main = async () => {

  testDoubleMap()
  testMultipleMap()

  process.exit()
}

main()
