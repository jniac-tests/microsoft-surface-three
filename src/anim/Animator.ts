import { resolveEase } from './Ease'

export class DoubleMap<U,V,W> {
  #map = new Map<U, Map<V,W>>()
  #size = 0
  get size() { return this.#size }
  has(target:U, key:V) {
    return this.#map.get(target)?.has(key) ?? false
  }
  get(target:U, key:V) {
    return this.#map.get(target)?.get(key)
  }
  /**
   * returns a clone Map if entry exists, or undefined otherwises.
   * This is not optimal, and should be used only for debug purposes.
   * For enumeration purposes prefer 'entries', 'getEntries'.
   * @param target
   * @returns 
   */
  getMap(target:U) {
    const map = this.#map.get(target)
    return map && new Map(map)
  }
  *entries() {
    for (const [target, map] of this.#map.entries()) {
      for (const [key, value] of map.entries()) {
        yield [target, key, value]
      }
    }
  }
  getEntries(target:U) {
    return this.#map.get(target)?.entries() ?? (function*(){})()
  }
  set(target:U, key:V, value:W) {
    const create = () => {
      const map = new Map<V,W>()
      this.#map.set(target, map)
      return map
    }
    const map = this.#map.get(target) ?? create()
    if (map.has(key) === false) {
      this.#size++
    }
    return map.set(key, value)
  }
  delete(target:U, key:V) {
    const map = this.#map.get(target)
    if (map === undefined) {
      return false
    }
    const deleted = map.delete(key)
    if (deleted) {
      this.#size--
    }
    if (map.size === 0) {
      this.#map.delete(target)
    }
    return deleted
  }
  deleteAll(target:U) {
    const map = this.#map.get(target)
    if (map !== undefined) {
      this.#size -= map.size
      map.clear()
      this.#map.delete(target)
      return true
    }
    return false
  }
  clear() {
    for (const map of this.#map.values()) {
      this.#size -= map.size
      map.clear()
    }
    this.#map.clear()
  }
}

export class MultipleMap<U,V> {
  #map = new Map<U, Set<V>>()
  #size = 0
  get size() { return this.#size }
  add(key:U, value:V) {
    const create = () => {
      const set = new Set<V>()
      this.#map.set(key, set)
      return set
    }
    const set = this.#map.get(key) ?? create()
    const beforeSize = set.size
    set.add(value)
    this.#size += set.size - beforeSize
    return this
  }
  get(key:U) {
    const set = this.#map.get(key)
    return set && new Set(set)
  }
  delete(key:U, value:V) {
    const set = this.#map.get(key)
    if (set === undefined) {
      return false
    }
    if (set.delete(value)) {
      if (set.size === 0) {
        this.#map.delete(key)
      }
      this.#size--
      return true
    }
    return false
  }
  deleteAll(key:U) {
    const set = this.#map.get(key)
    if (set !== undefined) {
      this.#size += -set.size
      set.clear()
    }
    return this.#map.delete(key)
  }
  clear() {
    for (const set of this.#map.values()) {
      this.#size = -set.size
      set.clear()
    }
    this.#map.clear()
  }
}

const getRequestAnimationFrame = () => {
  const start = Date.now()
  return ((cb:(time:number) => void) => {
    setTimeout(() => cb(Date.now() - start), 16)
  })
}
const requestAnimationFrame = globalThis.requestAnimationFrame ?? getRequestAnimationFrame()

const MAX_DELTA_TIME = 1 / 10
type Ticker = (dt:number) => boolean
const onTick = new Set<Ticker>()
const onCompleteMap = new Map<Ticker, Set<() => void>>()
const onCompleteAdd = (ticker:Ticker, cb:() => void) => {
  const create = () => {
    const set = new Set<() => void>()
    onCompleteMap.set(ticker, set)
    return set
  }
  (onCompleteMap.get(ticker) ?? create()).add(cb)
}
export let time = 0
export let frame = 0
let msOld = 0
const tick = (ms:number) => {

  const dt = Math.min((-msOld + (msOld = ms)) / 1000, MAX_DELTA_TIME)
  time += dt
  frame += 1

  const toRemove = new Set<Ticker>()
  for (const cb of onTick) {
    if (cb(dt)) {
      toRemove.add(cb)
    }
  }
  for (const cb of toRemove) {
    onTick.delete(cb)
    const onComplete = onCompleteMap.get(cb)
    if (onComplete !== undefined) {
      for (const cb of onComplete) {
        cb()
      }
    }
  }
  requestAnimationFrame(tick)
}
requestAnimationFrame(ms => {
  msOld = ms
  requestAnimationFrame(tick)
})



interface Timing {
  time:number
  timeOld:number
  duration:number
  progress:number
  complete:boolean
  canceled:boolean
}
export interface Interval {
  cancel:() => void
  then:(onfullfilled:(timing:Timing) => void) => void
  getTiming:() => Timing
}
export const during = (duration:number, cb:(interval:Interval) => void = () => {}):Interval => {
  let time = 0
  let timeOld = 0
  let complete = false
  let canceled = false
  const cancel = () => canceled = true
  const getTiming = () => {
    const progress = duration === 0 ? 1 : time / duration
    return { time, timeOld, duration, progress, complete, canceled }
  }
  const then = (onfullfilled:(timing:Timing) => void) => onCompleteAdd(ticker, () => onfullfilled(getTiming()))
  const interval = { cancel, then, getTiming }
  const ticker:Ticker = deltaTime => {
    timeOld = time
    time = Math.min(time + deltaTime, duration)
    complete = canceled || time === duration
    cb(interval)
    return complete
  }
  onTick.add(ticker)
  return interval
}



const tweenDoubleMap = new DoubleMap<object, string, Interval>()
interface Options {
  to:number
  from:number
  duration:number
  ease:string|((x:number) => number)
  onUpdate?:(interval:Interval) => void
}

// type TweenTarget = { [key:string]:any }|null
// more readable?
type TweenTarget = Record<string, unknown>

const ensureKeys = (keys:string|string[]) => {
  const split = (str:string) => str.trim().split(/\s*,\s*/).filter(key => !!key)
  if (Array.isArray(keys)) {
    return keys.filter(key => !!key).map(split).flat()
  }
  return split(keys)
}

export const tween = (target:TweenTarget, keys:string|string[], options:Partial<Options>|null = null) => {
  if (target === null || target === undefined) {
    return {
      intervals:[],
      then: (onfullfilled:() => void) => onfullfilled(),
    }
  }
  const intervals:Interval[] = []
  const ease = resolveEase(options?.ease ?? 'out3')
  for (const key of ensureKeys(keys)) {
    const { 
      from = Number(target[key] ?? 0), 
      to = Number(target[key] ?? 0),
      duration = 1,
      onUpdate,
    } = options ?? {}
    tweenDoubleMap.get(target, key)?.cancel()
    const interval = during(duration, interval => {
      const { progress, complete } = interval.getTiming()
      target[key] = from + (to - from) * ease(progress)
      onUpdate?.(interval)
      if (complete) {
        tweenDoubleMap.delete(target, key)
      }
    })
    tweenDoubleMap.set(target, key, interval)
    intervals.push(interval)
  }
  return {
    intervals,
    then: (onfullfilled:() => void) => Promise.all(intervals as any).then(() => onfullfilled()),
  }
}

export const killTweensOf = (target:TweenTarget, keys:string|string[] = '*') => {
  if (target === null || target === undefined) {
    return 0
  }
  let count = 0
  if (keys === '*') {
    for (const [, interval] of tweenDoubleMap.getEntries(target)) {
      interval.cancel()
      count++
    }
    tweenDoubleMap.deleteAll(target)
  } else {
    for (const key of ensureKeys(keys)) {
      const interval = tweenDoubleMap.get(target, key)
      if (interval) {
        interval.cancel()
        tweenDoubleMap.delete(target, key)
        count++
      }
    }
  }
  return count
}

export const animatorDebug = async () => {
  const Ease = await import('./Ease')
  const Animator = {
    tweenDoubleMap,
    during,
    tween,
    killTweensOf,
    Ease,
  }
  Object.assign(globalThis, { Animator })
}
