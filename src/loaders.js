import * as THREE from 'three'
import { EXRLoader } from 'three/examples/jsm/loaders/EXRLoader'
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader'

let pmremGenerator = null
/**
 * Returns the pmremGenerator initialized.
 * @returns {THREE.PMREMGenerator}
 */
const initPMREMGenerator = (renderer) => {
  if (pmremGenerator === null) {
    pmremGenerator = new THREE.PMREMGenerator(renderer)
    pmremGenerator.compileEquirectangularShader()
  }
  return pmremGenerator
}

const exrLoader = new EXRLoader().setDataType(THREE.UnsignedByteType)
const rgbeLoader = new RGBELoader().setDataType(THREE.UnsignedByteType)

/**
 * Get a loader, according to url's extension.
 * @param {string} url 
 * @returns {THREE.DataTextureLoader}
 */
export const getLoader = (url) => {
  const [,ext] = url.match(/\.(\w*)$/) || []
  if (ext === 'exr') {
    return exrLoader
  }
  if (ext === 'hdr') {
    return rgbeLoader
  }
  throw new Error(`oops, unknown extension "${ext}" (${url})`)
}

export const loadCubeMap = async (url, renderer) => {
  const texture = await getLoader(url).loadAsync(url)
  const renderTarget = initPMREMGenerator(renderer).fromEquirectangular(texture)
  return renderTarget.texture
}
