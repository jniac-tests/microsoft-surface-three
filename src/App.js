import { Canvas } from 'react-three-fiber'
import './App.css'
import { Stage } from './Stage'

function App() {
  return (
    <div className="App">
      <Canvas pixelRatio={devicePixelRatio}>
        <Stage/>
      </Canvas>
    </div>
  )
}

export default App
